"""update

Revision ID: 1edc53b960ff
Revises: 00000000
Create Date: 2017-11-26 23:56:37.248072

"""

# revision identifiers, used by Alembic.
revision = '1edc53b960ff'
down_revision = '00000000'

from sqlalchemy.sql import table, column
from alembic import op
import sqlalchemy as sa


def upgrade():
	op.execute('''UPDATE user SET point_balance = 1000 WHERE user_id = 2''')
	op.execute('''UPDATE user SET tier = 'Bronze' WHERE user_id = 3''')


def downgrade():
	op.execute('''UPDATE user SET point_balance = 0 WHERE user_id = 2''')
	op.execute('''UPDATE user SET tier = 'Carbon' WHERE user_id = 3''')