from app_main import app
from tests import OhmTestCase


class CommunityTest(OhmTestCase):

    def test_get(self):
        with app.test_client() as c:
            response = c.get('/community')
            assert (response.status_code == 200)
            assert ('Chuck Norris' in response.data)
            assert ('EUROPE' in response.data)
            assert ('+14086441234' in response.data)
            assert ('+14086445678' in response.data)
