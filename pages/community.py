from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User
from models._helpers import db


@app.route('/community', methods=['GET'])
def community():

    user = User.query.get(1)
    user.authenticated = True
    login_user(user, remember = True)

    results = db.engine.execute('''
        SELECT user.user_id, user.display_name, user.tier, user.point_balance, user.signup_date, 
            rel_user_multi.attribute, rel_user.attribute 
        FROM user 
        JOIN rel_user_multi ON rel_user_multi.user_id = user.user_id 
        JOIN rel_user ON rel_user.user_id = user.user_id 
        ORDER BY user.signup_date DESC
        LIMIT 5''').fetchall()

    users = {}

    for res in results:
        if res[0] not in users:
            users[res[0]] = {
                'display_name': res[1],
                'tier': res[2],
                'point_balance': res[3],
                'phone': [res[5]],
                'location': res[6]
                }
        else:
            users[res[0]]['phone'].append(res[5])

    args = {
       'users': users, 
    }

    return render_template("community.html", **args)

